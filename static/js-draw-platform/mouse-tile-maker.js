/**
 * A class that visualizes the mouse position within a tilemap. Call its update method from the
 * scene's update and call its destroy method when you're done with it.
 */
export default class MouseTileMarker {
  constructor(scene, map) {
    this.map = map;
    this.scene = scene;

    this.graphics = scene.add.graphics();
    this.graphics.lineStyle(5, 0xffffff, 1);
    this.graphics.strokeRect(0, 0, map.tileWidth, map.tileHeight);
    this.graphics.lineStyle(3, 0xaabbff, 1);
    this.graphics.strokeRect(0, 0, map.tileWidth, map.tileHeight);
    this.x = 0;
    this.y = 0;
  }

  update(player_pos) {
    const maxCursorDistance = 4;
    const pointer = this.scene.input.activePointer;
    const worldPoint = pointer.positionToCamera(this.scene.cameras.main);
    //conver the world cordinates to tile scale
    const pointerTileXY = this.map.worldToTileXY(worldPoint.x, worldPoint.y);
    const playerTileXY  = this.map.worldToTileXY(player_pos.x, player_pos.y);

    //figure out how far the cursor is from the player
    const y_diff = Math.abs(pointerTileXY.y - playerTileXY.y);
    const x_diff = Math.abs(pointerTileXY.x - playerTileXY.x);

    //hypotonuse
    const hyp = Math.sqrt(y_diff^2+x_diff^2);

    var y = 0;
    var x = 0;

    //if that distance is too large set new x and y that is only some distance
    if (y_diff > maxCursorDistance) {
      if (pointerTileXY.y <= playerTileXY.y) {
        y = playerTileXY.y - maxCursorDistance;
      } else {
        y = playerTileXY.y + maxCursorDistance;
      }
    } else {
      y = pointerTileXY.y;
    }
    if (x_diff > maxCursorDistance) {
      if (pointerTileXY.x <= playerTileXY.x) {
        x = playerTileXY.x - maxCursorDistance;
      } else {
        x = playerTileXY.x + maxCursorDistance;
      }
    } else {
      x = pointerTileXY.x;
    }

    //convert our tile map caclualtions to the world scale.
    const snappedWorldPoint = this.map.tileToWorldXY(x, y);
    this.graphics.setPosition(snappedWorldPoint.x, snappedWorldPoint.y);
    this.x = snappedWorldPoint.x;
    this.y = snappedWorldPoint.y;
  }

  destroy() {
    this.graphics.destroy();
  }
}
