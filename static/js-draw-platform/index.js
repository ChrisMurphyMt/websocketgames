/**
 * Author: Michael Hadley, mikewesthad.com
 * Asset Credits:
 *  - Tileset by 0x72 under CC-0, https://0x72.itch.io/16x16-industrial-tileset
 */

import PlatformerScene from "./platformer-scene.js";

const config = {
  type: Phaser.AUTO,
  parent: "game-container",
  pixelArt: true,
  backgroundColor: "#ff7355",
  scene: PlatformerScene,
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 800 }
    }
  },
  scale: {
    parent: 'game-container',
    mode: Phaser.Scale.FIT,
    width: 800,
    height: 600
  }
};
//game-container
const game = new Phaser.Game(config);
