/**
 * A class that wraps up our 2D platforming player logic. It creates, animates and moves a sprite in
 * response to WASD/arrow keys. Call its update method from the scene's update and call its destroy
 * method when you're done with the player.
 */
export default class Player {
  constructor(scene, x, y) {
    this.scene = scene;

    this.canDoubleJupmp = false;

    // Create the animations we need from the player spritesheet
    const anims = scene.anims;
    anims.create({
      key: "player-idle",
      frames: anims.generateFrameNumbers("player", { start: 0, end: 3 }),
      frameRate: 3,
      repeat: -1
    });
    anims.create({
      key: "player-jump",
      frames: anims.generateFrameNumbers("player", { star: 8, end: 8}),
      frameRate:1,
      repeat: -1
    })
    anims.create({
      key: "player-run",
      frames: anims.generateFrameNumbers("player", { start: 8, end: 15 }),
      frameRate: 12,
      repeat: -1
    });

    // Create the physics-based sprite that we will move around and animate
    this.sprite = scene.physics.add
      .sprite(x, y, "player", 0)
      .setDrag(1000, 0)
      .setMaxVelocity(300, 400)
      .setSize(18, 24)
      .setOffset(7, 9);

    // Track the arrow keys & WASD
    const { LEFT, RIGHT, UP, W, A, D, SPACE, SHIFT } = Phaser.Input.Keyboard.KeyCodes;
    this.keys = scene.input.keyboard.addKeys({
      left: LEFT,
      right: RIGHT,
      up: UP,
      w: W,
      a: A,
      d: D,
      space: SPACE,
      shift: SHIFT
    });
  }

  freeze() {
    this.sprite.body.moves = false;
  }

  update() {
    const { keys, sprite } = this;
    const onGround = sprite.body.blocked.down;
    const acceleration = onGround ? 600 : 200;


    // Apply horizontal acceleration when left/a or right/d are applied
    if (keys.left.isDown || keys.a.isDown) {
      sprite.setAccelerationX(-acceleration);
      // No need to have a separate set of graphics for running to the left & to the right. Instead
      // we can just mirror the sprite.
      sprite.setFlipX(true);
    } else if (keys.right.isDown || keys.d.isDown) {
      sprite.setAccelerationX(acceleration);
      sprite.setFlipX(false);
    } else {
      sprite.setAccelerationX(0);
    }

    // Only allow the player to jump if they are on the ground
    if ((onGround || this.canDoubleJupmp) && keys.space.isDown) {

      // if (onGround || this.canDoubleJupmp &&  keys.space.isDown) {
      sprite.anims.play("player-jump", true);
      if (this.canDoubleJupmp == true) {
        this.canDoubleJupmp = false;
      }
      sprite.setVelocityY(-500);
    }

    // Update the animation/texture based on the state of the player
    if (onGround) {
      this.canDoubleJupmp = true;
    }
      if (sprite.body.velocity.x !== 0){
        sprite.anims.play("player-run", true);
      }
      else {
        sprite.anims.play("player-idle", true);
      }
    // } else {
    //   // sprite.anims.stop();
    //   // sprite.setTexture("player", 10);
    //   sprite.anims.play("player-run", true)
    // }
  }

  destroy() {
    this.sprite.destroy();
  }
}
