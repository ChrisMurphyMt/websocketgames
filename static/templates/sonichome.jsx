const AUTH0_CLIENT_ID = "XgK06IwngvfdOJ13QvaX5Mvs5H3k3P68";
const AUTH0_DOMAIN = "sonichood.auth0.com";
const AUTH0_CALLBACK_URL = location.href;
const AUTH0_API_AUDIENCE = "http://sonichood.com/";
class App extends React.Component {
    parseHash() {
        this.auth0 = new auth0.WebAuth({
            domain: AUTH0_DOMAIN,
            clientID: AUTH0_CLIENT_ID
        });
        this.auth0.parseHash(window.location.hash, (err, authResult) => {
            if (err) {
                return console.log(err);
            }
            if (
                authResult !== null &&
                authResult.accessToken !== null &&
                authResult.idToken !== null
            ) {
                localStorage.setItem("access_token", authResult.accessToken);
                localStorage.setItem("id_token", authResult.idToken);
                localStorage.setItem(
                    "profile",
                    JSON.stringify(authResult.idTokenPayload)
                );
                window.location = window.location.href.substr(
                    0,
                    window.location.href.indexOf("#")
                );
            }
        });
    }
    setup() {
        $.ajaxSetup({
            beforeSend: (r) => {
                if (localStorage.getItem("access_token")) {
                    r.setRequestHeader(
                        "Authorization",
                        "Bearer " + localStorage.getItem("access_token")
                    );
                }
            }
        });
    }
    setState() {
        let idToken = localStorage.getItem("id_token");
        if (idToken) {
            this.loggedIn = true;
        } else {
            this.loggedIn = false;
        }
    }
    componentWillMount() {
        this.setup();
        this.parseHash();
        this.setState();
    }
    render() {
        if (this.loggedIn) {
            return <SonicHood loggedin={true}/>;
        }
        return <SonicHood Loggedin={false}/>;
    }
}

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.authenticate = this.authenticate.bind(this);
    }
    authenticate() {
        this.WebAuth = new auth0.WebAuth({
            domain: AUTH0_DOMAIN,
            clientID: AUTH0_CLIENT_ID,
            scope: "openid profile",
            audience: AUTH0_API_AUDIENCE,
            responseType: "token id_token",
            redirectUri: AUTH0_CALLBACK_URL
        });
        this.WebAuth.authorize();
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-xs-8 col-xs-offset-2 jumbotron text-center">
                        <h1>theSonicHood</h1>
                        <p>A Place for things we have built.</p>
                        <p>Sign in to get access </p>
                        <a
                            onClick={this.authenticate}
                            className="btn btn-primary btn-login btn-block"
                        >
                            Sign In
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

class SonicHood extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bands: []
        };
        this.serverRequest = this.serverRequest.bind(this);
        this.logout = this.logout.bind(this);
    }
    logout() {
        localStorage.removeItem("id_token");
        localStorage.removeItem("access_token");
        localStorage.removeItem("profile");
        location.reload();
    }
    serverRequest() {
        $.get("http://localhost:3000/bands", res => {
            this.setState({
                bands: res
            });
        });
    }
    componentDidMount() {
        this.serverRequest();
    }
    render() {
        var fun = this.logout();


        return (
            <div className="container">
                <br />
                <span className="pull-right">
                  <a onClick={this.logout}>Log out</a>
                </span>
                <h2>The Sonic Neighborhood</h2>
                <p>A Place for things we have made.</p>
                <div className="row">
                    <div className="container">
                        {this.state.bands.map(function(band, i) {
                            console.log(band);
                            return <Band key={i} band={band} />;
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

class Band extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (
            <div className="col-xs-4">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        #{this.props.band.id}{" "}
                    </div>
                    <div className="panel-body">{this.props.band.name}</div>
                </div>
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'));
