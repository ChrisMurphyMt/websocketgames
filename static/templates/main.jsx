const AUTH0_CLIENT_ID = "XgK06IwngvfdOJ13QvaX5Mvs5H3k3P68";
const AUTH0_DOMAIN = "sonichood.auth0.com";
const AUTH0_CALLBACK_URL = location.href;
const AUTH0_API_AUDIENCE = "http://sonichood.com/";
const PORT = "8080";
class App extends React.Component {
  parseHash() {
    this.auth0 = new auth0.WebAuth({
      domain: AUTH0_DOMAIN,
      clientID: AUTH0_CLIENT_ID
    });
    this.auth0.parseHash(window.location.hash, (err, authResult) => {
      if (err) {
        return console.log(err);
      }
      if (
        authResult !== null &&
        authResult.accessToken !== null &&
        authResult.idToken !== null
      ) {
        localStorage.setItem("access_token", authResult.accessToken);
        localStorage.setItem("id_token", authResult.idToken);
        localStorage.setItem(
          "profile",
          JSON.stringify(authResult.idTokenPayload)
        );
        //document.cookie = "profile=" + JSON.stringify(authResult.idTokenPayload);
        window.location = window.location.href.substr(
          0,
          window.location.href.indexOf("#")
        );
      }
    });
  }
  setup() {
    $.ajaxSetup({
      beforeSend: (r) => {
        if (localStorage.getItem("access_token")) {
          r.setRequestHeader(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        }
      }
    });
  }
  setState() {
    let idToken = localStorage.getItem("id_token");
    if (idToken) {
      this.loggedIn = true;
    } else {
      this.loggedIn = false;
    }
  }
  componentWillMount() {
    this.setup();
    this.parseHash();
    this.setState();
  }
  render() {
    if (this.loggedIn) {
      return <LoggedIn />;
    }
    return <Home />;
  }
}

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.authenticate = this.authenticate.bind(this);
  }
  authenticate() {
    this.WebAuth = new auth0.WebAuth({
      domain: AUTH0_DOMAIN,
      clientID: AUTH0_CLIENT_ID,
      scope: "openid profile",
      audience: AUTH0_API_AUDIENCE,
      responseType: "token id_token",
      redirectUri: AUTH0_CALLBACK_URL
    });
    this.WebAuth.authorize();
  }
  render() {
    return (
      <div className="container mainContent">
        <div className="row">
          <div className="col-xs-8 col-xs-offset-2 jumbotron">
            <a
              onClick={this.authenticate}
              className="btn btn-primary btn-login float-right"
             >
              Sign In
            </a>
          </div>
        </div>
      </div>
    );
  }
}

class LoggedIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bands: []
    };
    this.serverRequest = this.serverRequest.bind(this);
    this.logout = this.logout.bind(this);
  }
  logout() {
    localStorage.removeItem("id_token");
    localStorage.removeItem("access_token");
    localStorage.removeItem("profile");
    location.reload();
  }
  serverRequest() {

  	//http://sonichood-221819.appspot.com/
  	//localhost:3000
    $.get("http://localhost:" + PORT + "/bands", res => {
      this.setState({
        bands: res
      });
    });
  }
  componentDidMount() {
    this.serverRequest();
  }
  render() {
    return (
      <div className="container">
        <br />
        <span className="pull-right">
          <a onClick={this.logout}>Log out</a>
        </span>
        <p>
            these are the Artists
        </p>

        <div className={"row"}>
        {this.state.bands.map(function(band, i) {
          return <Band key={i} band={band} />;
        })}
        </div>
      </div>
    );
  }
}




class BandEditForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bandName: props.bandname,
            address1: props.address1,
            address2: props.address2,
            city: props.city,
            state: props.state,
            zip: props.zip,
            mainuser: props.mainuser
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }
    handleSubmit(event) {
        alert('gonna update with this info: ' + this.state.bandName);

        let b = {
            name: this.state.bandName,
            id: this.props.band.id,
            address: {
                address1:this.state.address1,
                address2: this.state.address2,
                city: this.state.city,
                state: this.state.state,
                zip: this.state.zip
            },
            mainuser: this.state.mainuser.trim()
        }
        let stringB = JSON.stringify(b);
        let dinkerFlinker = "http://localhost:" + PORT + "/postband/" + stringB;
        console.log(dinkerFlinker);
        $.post(
            dinkerFlinker,
            res => {
                console.dir(res);
                this.props.setDetailsFalse;
            }

        );

        event.preventDefault();
    }
    render() {
        let xs4 = "col-xs-4 col-sm-4"
        let xs8 = "col-xs-8 col-sm-8"
        return (
            <div className={"editForm "}>
            <form onSubmit={this.handleSubmit}>
                <label className={xs4}>
                    Band Name:
                </label>

                <input className={xs8}
                    name="bandName"
                    type="text"
                    value={this.state.bandName}
                    onChange={this.handleInputChange} />

                <label className={xs4}>
                    city:
                </label>
                <input
                    className={xs8}
                    name="city"
                    type="text"
                    value={this.state.city}
                    onChange={this.handleInputChange} />
                <label className={xs4}>
                    state:
                </label>
                <input
                    className={xs8}
                    name="state"
                    type="text"
                    value={this.state.state}
                    onChange={this.handleInputChange} />
                <input type="submit" value="Submit" />
            </form>
            </div>
        );
    }
}

class Band extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            band: props.band
        };
        this.serverRequest = this.serverRequest.bind(this);
        this.viewBand = this.viewBand.bind(this);
    }
    viewBand() {
        let b = this.props.band.id;
        this.serverRequest(b);

    }
    serverRequest() {
        var dinkerFlinker = "http://localhost:" + PORT + "/band/" + this.state.band.name

        $.post(
            dinkerFlinker,
            res => {
                console.dir(res);
                this.setState({detail: true});
                this.setState({band: res});
            }
        );
    }
    render() {
        let fork = localStorage.getItem("profile");
        let forkObj = JSON.parse(fork);

        if (this.state.band.mainuser.trim() === forkObj.sub.trim() && this.state.detail === true){
            return (<BandEditForm
                bandname={this.state.band.name}
                city={this.state.band.address.city}
                state={this.state.band.address.state}
                mainuser={this.state.band.mainuser}
                band={this.state.band}
                setDetailFalse={() => this.setState({detail: false})}
                    />)
        }
        if (this.state.detail === true) {
            return (
                <div className="col-xs-12 col-md-6 col-lg-4">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <div className="panel-body">{this.props.band.name}</div>
                            <ul>
                                <li>{this.state.city}</li>
                                <li>{this.state.state}</li>
                            </ul>
                            <a onClick={() => this.setState({detail: false})}>hide</a>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="col-xs-12 col-md-6 col-lg-4">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <div className="panel-body">{this.state.band.name}</div>
                            <a onClick={this.viewBand}>view band</a>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

ReactDOM.render(<App />, document.getElementById('app'));
