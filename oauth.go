package main

//jwtMiddleWare is being used to help authenticate the api access.
import jwtmiddleware "github.com/auth0/go-jwt-middleware"

var jwtMiddleWare *jwtmiddleware.JWTMiddleware

//Response used to store json message
type Response struct {
	Message string `json:"message"`
}

// Jwks stores a slice of JSON Web Keys
type Jwks struct {
	Keys []JSONWebKeys `json:"keys"`
}

// JSONWebKeys used by OAUTH functions
type JSONWebKeys struct {
	Kty string   `json:"kty"`
	Kid string   `json:"kid"`
	Use string   `json:"use"`
	N   string   `json:"n"`
	E   string   `json:"e"`
	X5c []string `json:"x5c"`
}
