package sonicdb

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"

	"websocketgames/internal/app/human"
	_ "github.com/denisenkom/go-mssqldb"
	"github.com/google/uuid"
)

type Database struct {
	Name     string
	Db       *sql.DB
	Server   string
	Port     int
	User     string
	Password string
	Database string
	ctx      context.Context
}

func (d *Database) buildConnString() string {
	// Build connection string
	d.ctx = context.Background()
	connString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s;",
		d.Server, d.User, d.Password, d.Port, d.Database)
	return connString
}

func (d *Database) StartDatabaseConnection() {
	connString := d.buildConnString()
	var err error

	// Create connection pool
	d.Db, err = sql.Open("sqlserver", connString)
	if err != nil {
		log.Fatal("Error creating connection pool:", err.Error())
	}
	// Check if database is alive.
	err = d.Db.PingContext(d.ctx)
	if err != nil {
		log.Fatal("Error pinging database: " + err.Error())
	}
}

func (d *Database) CloseDatabase() {
	defer d.Db.Close()
}

func (d *Database) SetUser(u human.User) error {
	if d.CheckIfUserExists(u.UserName) == true {
		return errors.New("user name already exists")
	}
	tsql := fmt.Sprintf("INSERT INTO AspNetUsers (Id, Email, EmailConfirmed ,PasswordHash, UserName, PhoneNumberConfirmed, TwoFactorEnabled, LockOutEnabled, AccessFailedCount) VALUES ('%s', '%s', '0','%s', '%s', '0', '0',1, 0);", uuid.New(), u.Email, u.PassWord, u.UserName)
	rows, err := d.Db.QueryContext(d.ctx, tsql)
	defer rows.Close()
	return err
}

func (d *Database) CheckIfUserExists(us string) bool {
	tsql := fmt.Sprintf("SELECT COUNT(UserName) FROM AspNetUsers WHERE UserName = '%s';", us)
	rows, err := d.Db.QueryContext(d.ctx, tsql)

	if err != nil {
		println("QueryContext err: ", err.Error())
	}
	defer rows.Close()
	rows.Next()
	var i int
	err = rows.Scan(&i)
	if i > 0 {
		return true
	}
	return false
}

func (d *Database) GetUserByEmail(em string) human.User {
	var us human.User
	tsql := fmt.Sprintf("SELECT Email, PasswordHash, UserName FROM AspNetUsers WHERE Email = '%s';", em)
	rows, err := d.Db.QueryContext(d.ctx, tsql)
	if err != nil {
		println("QueryContext err: ", err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		// Get values from row.
		err = rows.Scan(&us.Email, &us.PassWord, &us.UserName)
		if err != nil {
			println("Error reading rows: " + err.Error())
		}
	}
	return us
}

func (d *Database) GetUserByUserName(em string) human.User {
	var us human.User
	tsql := fmt.Sprintf("SELECT Email, PasswordHash, UserName FROM AspNetUsers WHERE UserName = '%s';", em)
	rows, err := d.Db.QueryContext(d.ctx, tsql)
	if err != nil {
		println("QueryContext err: ", err.Error())
	}
	defer rows.Close()

	rows.Next()
	// Get values from row.
	err = rows.Scan(&us.Email, &us.PassWord, &us.UserName)
	if err != nil {
		println("Error reading rows: " + err.Error())
	}

	return us
}

func (d *Database) UpdateBand(b human.Band) error {
	query := fmt.Sprintf("UPDATE Band SET " +
		"BandName = '%s', address1 = '%s', address2 = '%s', City = '%s', State = '%s', Zip = '%s' " +
		"WHERE  BandID = %d", b.Name, b.Address1, b.Address2, b.City, b.State, b.ZipCode, b.ID)
	rows, err := d.Db.QueryContext(d.ctx, query)
	if err != nil {
		println("could not update band: ", err.Error())
		return err
	}
	defer rows.Close()
	return nil
}

func (d *Database) LikeBand(likes int, bandName string) error {
	query := fmt.Sprintf("UPDATE Band SET Likes = '%s' WHERE BandID = '%s;", likes, bandName )
	rows, err := d.Db.QueryContext(d.ctx, query)
	if err != nil {
		println("Could not find band " + err.Error())
	}
	defer rows.Close()
	return err
}

func (d *Database) GetBandByName(bandName string) (human.Band, error) {
	var band human.Band
	tsql := fmt.Sprintf("SELECT BandID, BandName, MainUser, City, State FROM Band WHERE BandName = '%s';", bandName)
	rows, err := d.Db.QueryContext(d.ctx, tsql)
	if err != nil {
		println("Error reading rows: " + err.Error())
	}

	defer rows.Close()
	rows.Next()
	err = rows.Scan(&band.ID, &band.Name, &band.MainUser, &band.City, &band.State)
	if err != nil {
		println("Error reading rows: " + err.Error())
	}
	return band, err

}

func (d *Database) GetBands(rowCount int8) ([]human.Band, error) {
	bands := make([]human.Band, 0)
	tsql := fmt.Sprintf("SELECT TOP %d BandID, MainUser, BandName, address1, address2, City, State, Zip, Likes FROM Band;", rowCount)

	// Execute query
	rows, err := d.Db.QueryContext(d.ctx, tsql)
	if err != nil {
		println("Error reading rows: " + err.Error())
	}

	defer rows.Close()

	count := 0
	// Iterate through the result set.
	for rows.Next() {
		var b human.Band


		// Get values from row.
		err := rows.Scan(&b.ID, &b.MainUser, &b.Name, &b.Address1, &b.Address2,&b.City, &b.State, &b.ZipCode, &b.Likes)
		if err != nil {
			log.Fatal("Error reading rows: " + err.Error())
		}

		bands = append(bands, b)
		count++
	}

	return bands, err
}
