package human

import "regexp"

const (
	FoodNetwork = "Food Network"
	KingAurthur = "Kind Aurthur Flour"
)

//The below slices are used to parse each ingredient item


var LiquidUnitTypes = []string{"cup", "pint", "quart", "gallon"}
var DryUnitTypes = []string{"teaspoon", "tablespoon", "pound", "tsp", "tspn", "tbsp", "ounce", "oz"}
var NonStandardTypes = []string{"clove", "bunch", "jar", "head", "Pinch", "Splash", "slices", "sprig", "stalks", "head", "handful", "ear",
	"piece"}

var AllUnitTypes = [][]string{LiquidUnitTypes, DryUnitTypes, NonStandardTypes}

var FractionRegExs = []string{
	`((?P<int1>[0-9]*)\s*)((?P<num1>[0-9]+)/(?P<don1>[0-9]+))*\s+to\s+((?P<int2>[0-9]*)\s*)((?P<num2>[0-9]+)/(?P<don2>[0-9]+))*\s(?P<words>[\w*\W*]+)`,
	`(?P<int1>[0-9]*)\s+(?P<num1>[0-9]+)/(?P<don1>[0-9]+)\s(?P<words>[\w*\W*]+)`,
	`(?P<num1>[0-9]+)/(?P<don1>[0-9]+)\s+(?P<words>[\w*\W*]+)`,
	`(?P<int1>[0-9]+)\s+(?P<words>[\w*\W*]+)`}

type recipeFactory struct {
	Name           string
	Desc           string
	LiquidUnitType []string
	DryUnitType    []string
}

var SizeDescriptorTypes = []string{"small", "medium", "large"}

func RecipeFactory(params ...string) recipeFactory {
	rf := recipeFactory{}
	rf.LiquidUnitType = LiquidUnitTypes
	rf.DryUnitType = DryUnitTypes
	rf.Name = params[0]
	rf.Desc = params[1]
	return rf
}

func (rf *recipeFactory) ParseUnitOfMeasurement(str string) string {
	for units := range AllUnitTypes {
		for unit := range AllUnitTypes[units] {
			u := LiquidUnitTypes[unit]
			reg := "(?P<" + u + ">" + u + ")"
			match, _ := regexp.MatchString(reg, str)
			if match {
				return u
			}
		}
	}
	return "unknown"
}


func (rf *recipeFactory) ParsePotentialFraction(str string, reg string) map[string]string {

	compRegEx := regexp.MustCompile(reg)
	match := compRegEx.FindStringSubmatch(str)

	paramsMap := make(map[string]string)
	for i, name := range compRegEx.SubexpNames() {
		if i > 0 && i <= len(match) {
			paramsMap[name] = match[i]
		}
	}
	return paramsMap
}
