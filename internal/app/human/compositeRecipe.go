package human

import (
	"encoding/json"
	"fmt"
	"golang.org/x/net/html"
	"google.golang.org/api/customsearch/v1"
	"google.golang.org/api/googleapi/transport"
	"log"
	"net/http"
	"os"
	"sync"
)

type RecipeIngredient struct {
	Quantity          float32
	QuantityUpperValue float32
	UnitOfMeasurement string
	Descriptor        string
	Ingredient 		  string
}

type RecipeInstruction struct {
}

type FoodNetworkFullRecipe struct {
	Context          string `json:"@context"`
	Type             string `json:"@type"`
	MainEntityOfPage bool   `json:"mainEntityOfPage"`
	Name             string `json:"name"`
	URL              string `json:"url"`
	Headline         string `json:"headline"`
	Author           []struct {
		Type string `json:"@type"`
		Name string `json:"name"`
		URL  string `json:"url"`
	} `json:"author"`
	Image struct {
		Type   string `json:"@type"`
		URL    string `json:"url"`
		Height string `json:"height"`
		Width  string `json:"width"`
	} `json:"image"`
	DatePublished string `json:"datePublished"`
	DateModified  string `json:"dateModified"`
	Publisher     struct {
		Type string `json:"@type"`
		Name string `json:"name"`
		URL  string `json:"url"`
		Logo struct {
			Type   string `json:"@type"`
			URL    string `json:"url"`
			Height string `json:"height"`
			Width  string `json:"width"`
		} `json:"logo"`
	} `json:"publisher"`
	CookTime           string   `json:"cookTime"`
	TotalTime          string   `json:"totalTime"`
	RecipeIngredient   []string `json:"recipeIngredient"`
	RecipeInstructions []string `json:"recipeInstructions"`
	AggregateRating    struct {
		Type        string  `json:"@type"`
		RatingValue float64 `json:"ratingValue"`
		ReviewCount int     `json:"reviewCount"`
	} `json:"aggregateRating"`
	RecipeYield string `json:"recipeYield"`
	Review      []struct {
		Type   string `json:"@type"`
		Author struct {
			Type string `json:"@type"`
			Name string `json:"name"`
		} `json:"author"`
		ReviewRating struct {
			Type        string `json:"@type"`
			RatingValue int    `json:"ratingValue"`
			WorstRating string `json:"worstRating"`
			BestRating  string `json:"bestRating"`
		} `json:"reviewRating,omitempty"`
		ReviewBody    string `json:"reviewBody"`
		DatePublished string `json:"datePublished"`
	} `json:"review"`
	RecipeCategory string `json:"recipeCategory"`
}

type CompositeRecipe struct {
	SearchTerm string
	RecipeCount int
	FoodNetRecipes []FoodNetworkFullRecipe
	mux sync.Mutex
}

func (r *CompositeRecipe) ParseMeasurementUnit(unit string) {

}

func (r *CompositeRecipe) RecipeHttpRequest(url string, ch chan FoodNetworkFullRecipe) {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("err: %s", err.Error())
	}
	b := resp.Body

	z := html.NewTokenizer(b)
	recipeFound := false

	for !recipeFound {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			recipeFound = true
		case tt == html.StartTagToken:
			t := z.Token()

			for _, a := range t.Attr {

				if a.Key == "type" && a.Val == "application/ld+json" {
					tt = z.Next()
					t := z.Token()
					b := FoodNetworkFullRecipe{}
					if err := json.Unmarshal([]byte(t.Data), &b); err == nil {
						if b.Name != "" {
							ch <- b
						}
					}
					recipeFound = true
					//break
				}
			}
		}
	}

	err = b.Close()
	if err != nil {
		log.Fatalf("could not close file: %+v", err)
	}
	close(ch)

}

func (r *CompositeRecipe) ProcessSearchQuery(searchQuery string, total int) {

	r.SearchTerm = searchQuery

	var apiKey = os.Getenv("GOOGLEAPIKEY")
	var cx = os.Getenv("CSEID")

	client := &http.Client{Transport: &transport.APIKey{Key: apiKey}}

	svc, err := customsearch.New(client)
	if err != nil {
		log.Fatal(err)
	}
	resp := svc.Cse.Siterestrict.List(searchQuery)
	resp.Cx(cx)


	foodNetworkFactory := RecipeFactory("Food Network", "a dope thing for stuff")


	pageSize := 10

	for i := 0; i <= total; i += pageSize {

		start := int64(i + 1)
		resp.Start(start)
		dif := total - i
		if dif < 10 {
			resp.Num(int64(dif))
		}

		res, err := resp.Do()
		if err != nil {
			log.Fatal(err)
		}

		for _, result := range res.Items {
			c := make(chan FoodNetworkFullRecipe)
			go r.RecipeHttpRequest(result.Link, c)

			for i := range c {
				r.FoodNetRecipes = append(r.FoodNetRecipes, i)
				for j := range i.RecipeIngredient {
					uhm := i.RecipeIngredient[j]
					println("\n", uhm)
					for r := range FractionRegExs {
						somting := foodNetworkFactory.ParsePotentialFraction(uhm, FractionRegExs[r])
						not := false
						//unit := foodNetworkFactory.ParseUnitOfMeasurement(uhm)
						switch  {
						case len(somting) == 7:
							for a := range somting {
								if ww := somting[a]; ww != "" {
									print(somting[a], " ")
								}
							}
							not = true
							println("double mixed............")
						case len(somting) == 4:

							for a := range somting {
								if ww := somting[a]; ww != "" {
									print(somting[a], " ")
								}
							}
							not = true
							println("mixed............")
						case len(somting) == 3:
							for a := range somting {
								if ww := somting[a]; ww != "" {
									print(somting[a], " ")
								}
							}
							not = true
							println("fraction............")
						case len(somting) == 2:
							for a := range somting {
								if ww := somting[a]; ww != "" {
									print(somting[a], " ")
								}
							}
							not = true
							println("int............")
						}
						if not == true {
							break
						}
					}
				}
			}
		}
	}
}