package human

type Profile struct {
	nickname string `json:"nickname" binding:"required"`
	name     string `json:"name" binding:"required"`
}

type Address struct {
	Address1 string `json:"address"`
	Address2 string `json:"address2"`
	City     string `json:"city"`
	State    string `json:"state"`
	ZipCode  string `json:"zipCode"`
}

//User is the base type to keep track of people
type User struct {
	UserName string `json:"username" binding:"required"`
	PassWord []byte `json:"password" binding:"required"`
	Email    string `json:"email" binding:"required"`
	LoggedIn bool   `json:"loggedin"`
}

type Band struct {
	User     `json:"user"`
	Address  `json:"address"`
	MainUser string `json:"mainuser"`
	Name     string `json:"name" binding:"required"`
	Likes    int    `json:"likes"`
	ID       int    `json:"id" binding:"required"`
}

func (d *Band) LikeSort() {

}

type LoginObj struct {
	User
	Token    string
	Redirect string
}
