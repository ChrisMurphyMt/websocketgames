// Copyright 2018 theSonicHood. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"github.com/kataras/iris"
	"github.com/kataras/iris/middleware/logger"
	"github.com/kataras/iris/middleware/recover"
	"github.com/kataras/iris/websocket"
)

var (
	cookieNameForSessionID = "SonicMain"
)

func serveHome(ctx iris.Context) {
	ctx.View("index.html")
}

func serverDrawingPlatformer(ctx iris.Context) {
	ctx.View("drawing-platformer/drawing-platformer.html")
}

func serveMMO(ctx iris.Context) {
	// http.ServeFile(w, r, "./views/mmo.gtpl")
	ctx.View("mmo.html")
}

//even favicons need handlers
func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "favicon.ico")
}

func main() {

	jwtMiddleware := jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			aud := os.Getenv("AUTH0_API_AUDIENCE")
			iss := os.Getenv("AUTH0_DOMAIN")

			checkAudience := token.Claims.(jwt.MapClaims).VerifyAudience(aud, false)
			if !checkAudience {
				return token, errors.New("invalid audience.")
			}
			//verify iss claim

			checkIss := token.Claims.(jwt.MapClaims).VerifyIssuer(iss, false)
			if !checkIss {
				return token, errors.New("invalid issuer")
			}
			cert, err := getPemCert(token)
			if err != nil {
				log.Fatalf("could not get cert: %+v", err)
			}
			result, _ := jwt.ParseRSAPublicKeyFromPEM([]byte(cert))

			return result, nil
		},
		SigningMethod: jwt.SigningMethodRS256,
	})
	// register our actual jwtMiddleware
	jwtMiddleWare = jwtMiddleware

	ws := websocket.New(websocket.Config{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	})

	ws.OnConnection(handleConnection)
	app := iris.New()
	app.Logger().SetLevel("debug")
	// Optionally, add two built'n handlers
	// that can recover from any http-relative panics
	// and log the requests to the terminal.
	app.Use(recover.New())
	app.Use(logger.New())

	app.RegisterView(iris.Django("./templates/.", ".html").Reload(true))

	http.HandleFunc("/favicon.ico", faviconHandler)

	//Start up the websocket hub with a go routine
	flag.Parse()

	//All
	app.Get("/", serveHome)

	app.Get("/mmo/", serveMMO)
	app.Get("/gametime/", serverDrawingPlatformer)

	app.StaticWeb("/static/", "./static/")

	app.Any("/iris-ws.js", func(ctx iris.Context) {
		ctx.Write(websocket.ClientSource)
	})

	// Serve using a host:port form.
	var addr = iris.Addr(":8080")
	app.Run(addr)
}

// Functions need to authenticate api access
// authMiddleware intercepts the requests, and check for a valid jwt token
func authMiddleware(c iris.Context) {
	// Get the client secret key
	err := jwtMiddleWare.CheckJWT(c.ResponseWriter(), c.Request())
	if err != nil {
		// Token not found
		fmt.Println(err)
		c.StopExecution()
		c.ResponseWriter().WriteHeader(http.StatusUnauthorized)
		c.ResponseWriter().Write([]byte("Unauthorized"))
	}
	something := jwtMiddleWare.Options.ValidationKeyGetter
	println("somthing: ", something)
	c.Next()
}

func getPemCert(token *jwt.Token) (string, error) {
	cert := ""
	resp, err := http.Get(os.Getenv("AUTH0_DOMAIN") + ".well-known/jwks.json")
	if err != nil {
		return cert, err
	}
	defer resp.Body.Close()
	var jwks = Jwks{}
	err = json.NewDecoder(resp.Body).Decode(&jwks)
	if err != nil {
		return cert, err
	}
	x5c := jwks.Keys[0].X5c
	for k, v := range x5c {
		if token.Header["kid"] == jwks.Keys[k].Kid {
			cert = "-----BEGIN CERTIFICATE-----\n" + v + "\n-----END CERTIFICATE-----"
		}
	}
	if cert == "" {
		return cert, errors.New("unable to find appropriate key.")
	}

	return cert, nil
}

//Functions for websocket connections
func handleConnection(c websocket.Connection) {
	// Read events from browser
	c.On("chat", func(msg string) {
		// Print the message to the console, c.Context() is the iris's http context.
		fmt.Printf("%s sent: %s\n", c.Context().RemoteAddr(), msg)
		// Write message back to the client message owner:
		// c.Emit("chat", msg)
		c.To(websocket.Broadcast).Emit("chat", msg)
	})
}
